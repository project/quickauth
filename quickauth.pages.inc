<?php

/**
 * Generate bookmarkable URLs for different use cases. e.g. iOs, Chrome
 */
function _quickauth_bookmarklets($bookmarklet) {
  $output = <<<eof
  
eof;

  return $output;
}

function quickauth_account_setup($form, &$form_state, $account) {
  global $base_url;
  // If storage is set it means a bookmarklet was generated, return early.
  if (!empty($form_state['storage'])) {
    $form['quickauth_description'] = array(
      '#markup' => '<p>' . t('Bookmark the appopriate link for your browser to setup QuickAuth.') . '</p>',
    );
    // Don't run through URL because it will strip the javascript.
    $form['quickauth_ff_bookmarklet'] = array(
      '#markup' => "<dl><dt>Firefox</dt><dd><a href=\"" . $form_state['storage']['bookmarklet'] ."\">" . t('Bookmarkable QuickAuth URL') . "</a></dd>", 
    );
    $click = t('Click, then bookmark and edit');
    $edit = t('to remove everything before "javascript" e.g. from http://example.org/#javascript remove http://example.org/#');
    $form['quickauth_chrome_bookmarklet'] = array(
      '#markup' => "<dt>Chrome</dt><dd><a href=\"" . "#" . $form_state['storage']['bookmarklet'] ."\">" . $click . "</a> " . $edit . "</dd>",
    );
    $form['quickauth_hash_bookmarklet'] = array(
      '#markup' => "<dt>iOS (iPhone, iPad)</dt><dd><p>" . t('Bookmark this current page then copy the following code and replace the saved bookmark URL with the code.') . "</p><textarea rows='4' cols='51'>" . $form_state['storage']['bookmarklet'] . "</textarea></dd>",
    );
    $form['quickauth_help'] = array(
      '#markup' => '<p>' . t('Browsers provide different methods for bookmarking URLs. For Safari on iOS devices (iPhone) for example, you must edit the saved bookmark and replace the URL with the Javascript.') . '</p>',
    );
    return $form;
  }

  // If account secret is already set warn about regenerating.
  if (!quickauth_get_account_secret($account)) {
    $description = t('To use QuickAuth, click the button and be sure to bookmark the resulting link.');
    $button_value = t('Generate bookmarklet');
  }
  else {
    $description = t('You have already generated a QuickAuth bookmarklet. Re-generating one will erase the current one so be sure to bookmark the new link if you continue.');
    $button_value = t('Re-generate bookmarklet');
  }
  $form['account'] = array('#type' => 'value', '#value' => $account);
  $form['quickauth_description'] = array(
    '#markup' => '<p>' . $description . '</p>',
  );
  $form['quickauth_generate'] = array(
    '#type' => 'submit',
    '#value' => $button_value,
  );
  return $form;
}

function quickauth_account_setup_submit($form, &$form_state) {
  // Trigger multistep.
  $form_state['rebuild'] = TRUE;
  $account = $form_state['values']['account'];
  $bookmarklet = quickauth_generate_bookmarklet($account);
  $form_state['storage']['bookmarklet'] = $bookmarklet;
}
