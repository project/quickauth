Authentication URLs for Drupal.

### Feature backlog

 * Per-browser bookmarklets
 * Email tokens for authentication
   * Will require implementing hook_boot()
 * Consider dis-allowing regular login form and place it behind QuickAuth URLs